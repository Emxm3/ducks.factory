﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory._composition
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AutoWireAttribute : Attribute
    {
        public bool AsSingleton { get; } //When autowiring, wire this up as a singleton
        public AutoWireAttribute(bool AsSingleton = false)
        {
            this.AsSingleton = AsSingleton;
        }
    }
}
