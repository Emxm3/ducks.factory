﻿using Ducks.Factory.MVVM.Model;
using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.ViewModel.Catalogue;
using Ducks.Factory.MVVM.ViewModel.MainWindow;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Ducks.Factory._composition
{
    public class CompositionRoot : BaseRoot
    {
        [STAThread]
        public static void Main()
        {
            new CompositionRoot().Execute<MainWindow, IMainWindowViewModel>();
        }

        public override void CustomInjections(Container container)
        {
            container.Options.ResolveUnregisteredConcreteTypes = true;
            var existingAssemblies = new List<Assembly>() { GetType().Assembly };
            var asSingleton = Lifestyle.Singleton;

            container.RegisterInstance<Func<Duck, ICatalogueDuck>>((d) => 
            {
                ICatalogueDuck cat = container.GetInstance<ICatalogueDuck>();
                cat.Duck = d;
                return cat;
            });

           

            //Register ducks in all one assemblies, and assign them as singleton
            container.Collection.Register<Duck>(existingAssemblies, asSingleton);


            //Register ponds in all one assemblies, and assign them as singleton
            container.Collection.Register<Pond>(existingAssemblies, asSingleton);
        }
    }
}
