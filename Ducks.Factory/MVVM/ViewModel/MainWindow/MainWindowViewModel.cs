﻿using Ducks.Factory._composition;
using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model;
using Ducks.Factory.MVVM.Model.Behaviour;
using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Ponds;
using Ducks.Factory.MVVM.ViewModel.Catalogue;
using Microsoft.Expression.Interactivity.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ducks.Factory.MVVM.ViewModel.MainWindow
{
    

    [AutoWire(AsSingleton: true)]
    public class MainWindowViewModel : ObservableClass, IMainWindowViewModel
    {
        private ObservableCollection<Pond> _ListOfPonds;
        private bool _Loaded = false;
        private ObservableCollection<Duck> _ListOfDucks;
        private ObservableCollection<ICatalogueDuck> _DuckCatalouge;
        private readonly IEnumerable<Pond> ponds;
        private readonly IEnumerable<Duck> ducks;
        private readonly Func<Duck, ICatalogueDuck> catalogueDuckFactory;
        private Duck _TargetDuck;

        public ObservableCollection<Pond> ListOfPonds { get => _ListOfPonds ??= new(); set => SetPropertyValue(ref _ListOfPonds, value); }
        public ObservableCollection<Duck> ListOfDucks { get => _ListOfDucks ??= new(); set => SetPropertyValue(ref _ListOfDucks, value); }

        public ObservableCollection<ICatalogueDuck> DuckCatalouge { get => _DuckCatalouge ??= new(); set => SetPropertyValue(ref _DuckCatalouge, value); }

        public ICommand Initialize => new ActionCommand(() => _Initialize());
        public ICommand Clear => new ActionCommand(() => _Clear());
        private void _Clear()
        {
            ListOfDucks.Clear();
            NotifyPropertyChanged(nameof(ListOfDucks));
        }

        public bool Loaded { get => _Loaded; set => SetPropertyValue(ref _Loaded, value); }
        public IMagicalFarAwayLand MagicalFarAwayLand { get; }
        public IFarm Farm { get; }
        public Duck TargetDuck { get => _TargetDuck; set => SetPropertyValue(ref _TargetDuck, value); }

        private void _Initialize()
        {
            DuckCatalouge = new(ducks.Select(catalogueDuckFactory));
            DuckCatalouge.ToList().ForEach(d => d.OnDuckSelected += OnDuckSelected);
            //LISTEN TO ALL EVENTS:
            foreach (var pond in ponds)
                pond.EmitDuck += GetDuck;

            MagicalFarAwayLand.OnCreation += GetDuck;
            Farm.OnTransmit += GetDuck;

            Task.Factory.StartNew(LoadPonds); //Load the ducks, then hide the loading bar

            TargetDuck = ducks.FirstOrDefault(d => d.GetType() == typeof(Mallard)); //We want to look for Mallards
        }

        private void OnDuckSelected(object sender, Duck e)
        {
            TargetDuck = e;
        }

        private void LoadPonds()
        {
            //Update their Strategies. This current implementation is bad design but we'll deal with it later.
            ListOfPonds.ToList().ForEach(p => { });
            NotifyPropertyChanged(nameof(ListOfPonds));
            Loaded = true;
        }

        public MainWindowViewModel(
            IEnumerable<Pond> Ponds
            , IMagicalFarAwayLand magicalFarAwayLand
            , IFarm farm
            , IEnumerable<Duck> ducks
            , Func<Duck, ICatalogueDuck> catalogueDuckFactory)
        {

            //Notice we are only storing dependencies with minor transforms. 
            //We don't want to instantiate or execute any of our dependencies during this step
            ListOfPonds = new ObservableCollection<Pond>(Ponds);
            ponds = Ponds;
            MagicalFarAwayLand = magicalFarAwayLand;
            Farm = farm;
            this.ducks = ducks;
            this.catalogueDuckFactory = catalogueDuckFactory;
        }

        /// <summary>
        /// Executes when a source event is raised and broadcasts a duck
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="duck"></param>
        private void GetDuck(object sender, Duck duck)
        {
            ListOfDucks.Add(duck);
            NotifyPropertyChanged(nameof(ListOfDucks));
        }
    }

    public interface IMainWindowViewModel
    {
        ObservableCollection<ICatalogueDuck> DuckCatalouge { get; set; }
        IFarm Farm { get; }
        ICommand Initialize { get; }
        ObservableCollection<Duck> ListOfDucks { get; set; }
        ObservableCollection<Pond> ListOfPonds { get; set; }
        bool Loaded { get; set; }
        IMagicalFarAwayLand MagicalFarAwayLand { get; }
        Duck TargetDuck { get; set; }
    }
}
