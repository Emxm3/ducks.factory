﻿using Ducks.Factory._composition;
using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model.Ducks;
using Microsoft.Expression.Interactivity.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ducks.Factory.MVVM.ViewModel.Catalogue
{
    [AutoWire]
    public class CatalogueDuck : ObservableClass, ICatalogueDuck
    {
        private Duck _Duck;

        // The type of duck this item catalogues
        public Duck Duck { get => _Duck; set => SetPropertyValue(ref _Duck, value); }

        //The action to perform when selected
        public ICommand Selected => new ActionCommand(() => OnDuckSelected?.Invoke(this,Duck));

        public event EventHandler<Duck> OnDuckSelected;
    }

    public interface ICatalogueDuck
    {
        event EventHandler<Duck> OnDuckSelected;
        Duck Duck { get; set; }
        ICommand Selected { get; }
    }
}
