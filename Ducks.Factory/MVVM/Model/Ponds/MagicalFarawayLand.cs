﻿using Ducks.Factory._composition;
using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Strategies.Flight;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using Microsoft.Expression.Interactivity.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ducks.Factory.MVVM.Model.Ponds
{
    [AutoWire(AsSingleton:true)]
    public class MagicalFarAwayLand : IMagicalFarAwayLand
    //Unicorns and kittens!
    {
        public event EventHandler<Duck> OnCreation;

        public ICommand MagicallyAppear => new ActionCommand(() => _MagicallyAppear());

        public string Land => "~*~ Magical Far Away Land ~*~";

        private void _MagicallyAppear()
        {
            ManifestDuckFromRainbows();
        }

        public void ManifestDuckFromRainbows()
        {
            Mallard magicDuck = new Mallard(
                new RideTheRainbowFlightStrategy(), //Magical!
                new SpewRainbowsSoundStrategy()     //Trying to get away with things?
            );

            OnCreation?.Invoke(this, magicDuck);
        }
    }

    public interface IMagicalFarAwayLand
    {
        public string Land { get; }
        event EventHandler<Duck> OnCreation;
        ICommand MagicallyAppear { get; }

        void ManifestDuckFromRainbows();
    }
}
