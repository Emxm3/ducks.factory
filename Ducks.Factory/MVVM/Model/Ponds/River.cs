﻿using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Strategies.Flight;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ponds
{
    public class River : Pond
    {
  
        // While this implementation is right for the requirements of supplying mallards
        // it still bears the responsibility of constructing the Mallard from scratch.
        public override Duck BreedDuck()
        {
            IWingedFlightStrategy wings = new WingedFlightStrategy();
            IQuackSoundStrategy quack = new QuackSoundStrategy();

            Mallard duck = new Mallard(wings, quack); //Mallard as Mallard

            return duck;
        }
    }
}
