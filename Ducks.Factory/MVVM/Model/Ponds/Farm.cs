﻿using Ducks.Factory._composition;
using Ducks.Factory.MVVM.Model.Ducks;
using Microsoft.Expression.Interactivity.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ducks.Factory.MVVM.Model.Ponds
{
    [AutoWire(AsSingleton: true)]
    public class Farm : IFarm
    // This is not a pond (neither are a few others, but this is to show how to handle across unrelated types)
    {
        public event EventHandler<Duck> OnTransmit;
        public ICommand TransmitDuck => new ActionCommand(FarmDuck);

        public string Title => "Incompetent Farm";

        private void FarmDuck()
        {
            Duck duck = new Mallard(null, null); //Not following spec... I mean, It works...

            OnTransmit?.Invoke(this, duck);
        }

    }

    public interface IFarm
    {
        string Title { get; }
        ICommand TransmitDuck { get; }
        event EventHandler<Duck> OnTransmit;
    }
}
