﻿using Ducks.Factory.MVVM.Model.Behaviour;
using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Strategies.Flight;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ponds
{
    //Dip is trying to be smart. It is using Dependency Injection to determine what will get built
    public class Dip : Pond
    {
        public Mallard TargetDuck { get; }

        public Dip(IEnumerable<Duck> ducks)
        {
            TargetDuck = ducks.FirstOrDefault(d => d.GetType() == typeof(Mallard)) as Mallard;
        }


        public override Duck BreedDuck()
        {
            Mallard duck = new Mallard((IWingedFlightStrategy)TargetDuck.FlightStrategy, (IQuackSoundStrategy)TargetDuck.SoundStrategy);
            return duck;
        }

       
    }
}
