﻿using Ducks.Factory._composition;
using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model.Ducks;
using Microsoft.Expression.Interactivity.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ducks.Factory.MVVM.Model
{
    

    public abstract class Pond : ObservableClass, IPond
    {
        public event EventHandler<Duck> EmitDuck;


        public ICommand SendDuck => new ActionCommand(() => _SendDuck());

        private void _SendDuck()
        {
            Duck bredDuck = BreedDuck();
            EmitDuck?.Invoke(this, bredDuck);
        }

        public string Name => GetType().Name;

        public abstract Duck BreedDuck();

    }

    public interface IPond
    {
        string Name { get; }
        ICommand SendDuck { get; }
        event EventHandler<Duck> EmitDuck;
        Duck BreedDuck();
    }

}
