﻿using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Strategies.Flight;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ponds
{
    public class Lake : Pond
    {

        public override Duck BreedDuck()
        {
            IWingedFlightStrategy wings = new WingedFlightStrategy();
            IQuackSoundStrategy quack = new QuackSoundStrategy();

            Duck duck = new AmericanBlackDuck(wings, quack);

            return duck;
        }

    }
}
