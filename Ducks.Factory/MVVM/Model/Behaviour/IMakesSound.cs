﻿using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Behaviour
{
    public interface IMakesSound
    {
        ISoundStrategy SoundStrategy { get; }
        public string Sound { get; }
    }
}
