﻿using Ducks.Factory.MVVM.Model.Strategies.Flight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Behaviour

{
    public interface ICanFly
    {
        IFlightStrategy FlightStrategy { get; }
        string FlightDescription { get; }
    }
}
