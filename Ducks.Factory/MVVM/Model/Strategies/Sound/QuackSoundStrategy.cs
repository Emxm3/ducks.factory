﻿using Ducks.Factory._composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Strategies.Sound
{
    [AutoWire(AsSingleton: true)]
    public class QuackSoundStrategy : IQuackSoundStrategy
    {
        public string Execute() => "This duck quacks";
    }

    public interface IQuackSoundStrategy : ISoundStrategy
    {
    }
}
