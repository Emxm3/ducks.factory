﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Strategies.Sound
{
    public class SpewRainbowsSoundStrategy : ISpewRainbowsSoundStrategy, IQuackSoundStrategy
    {
        public string Execute()
        {
            return "** !!SPEWS RAINBOWS!! **";
        }
    }

    public interface ISpewRainbowsSoundStrategy : ISoundStrategy
    {
    }
}
