﻿namespace Ducks.Factory.MVVM.Model.Strategies
{
    public interface IStrategy
    {
        public string Execute();
    }
}