﻿using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Strategies.Flight
{
    public class RideTheRainbowFlightStrategy : IRideTheRainbowFlightStrategy, IWingedFlightStrategy
    {
        public string Execute()
        {
            return "RIDE THE RAINBOW!!!";
        }
    }

    public interface IRideTheRainbowFlightStrategy : IFlightStrategy
    {
    }
}
