﻿using Ducks.Factory._composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Strategies.Flight
{
    [AutoWire(AsSingleton: true)]
    public class WingedFlightStrategy : IWingedFlightStrategy
    {
        public string Execute() => "This duck flaps its wings";
    }


    public interface IWingedFlightStrategy : IFlightStrategy
    {
    }
}
