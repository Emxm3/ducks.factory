﻿using Ducks.Factory._composition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Strategies.Flight
{
    [AutoWire(AsSingleton: true)]
    public class RocketBoosterStrategy : IRocketBoosterStrategy
    {
        public string Execute()
        {

            return "Uses jet engines";
        }
    }

    public interface IRocketBoosterStrategy : IFlightStrategy
    {
    }
}
