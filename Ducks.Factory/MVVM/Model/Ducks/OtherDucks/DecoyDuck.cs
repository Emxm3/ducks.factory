﻿using Ducks.Factory.MVVM.Model.Behaviour;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks.OtherDucks
{
    public class DecoyDuck : Duck, IMakesSound
    {
        private string _Sound;
        public string Sound { get => _Sound ??= SoundStrategy?.Execute(); set => SetPropertyValue(ref _Sound, value); }

        public DecoyDuck(IQuackSoundStrategy quackSoundStrategy)
        {
            Name = "Decoy Duck";
            SoundStrategy = quackSoundStrategy;
        }

        public ISoundStrategy SoundStrategy { get; }
    }
}
