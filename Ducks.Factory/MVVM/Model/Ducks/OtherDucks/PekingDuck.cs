﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks.OtherDucks
{
    public class PekingDuck : Duck
    {

        public PekingDuck()
        {
            Name = "Peking Duck";
        }

    }
}
