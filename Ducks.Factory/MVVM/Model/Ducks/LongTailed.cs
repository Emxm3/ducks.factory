﻿using Ducks.Factory.MVVM.Model.Behaviour;
using Ducks.Factory.MVVM.Model.Strategies.Flight;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{
    public class LongTailed : Duck, ICanFly, IMakesSound
    {
        private string _FlightDescription;
        public string FlightDescription { get => _FlightDescription ??= FlightStrategy?.Execute(); set => SetPropertyValue(ref _FlightDescription, value); }

        private string _Sound;
        public string Sound { get => _Sound ??= SoundStrategy?.Execute(); set => SetPropertyValue(ref _Sound, value); }

        public LongTailed(IWingedFlightStrategy wingedFlightStrategy, IQuackSoundStrategy quackSoundStrategy)
        {
            Name = "Long-Tailed";

            FlightStrategy = wingedFlightStrategy;
            SoundStrategy = quackSoundStrategy;

        }

        public ISoundStrategy SoundStrategy { get; }

        public IFlightStrategy FlightStrategy { get; }
    }
}
