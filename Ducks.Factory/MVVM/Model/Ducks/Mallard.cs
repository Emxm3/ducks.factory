﻿using Ducks.Factory.MVVM.Model.Behaviour;
using Ducks.Factory.MVVM.Model.Strategies.Flight;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{
    public class Mallard : Duck, ICanFly, IMakesSound
    {
        private string _FlightDescription;
        public string FlightDescription { get => _FlightDescription ??= FlightStrategy?.Execute(); private set => SetPropertyValue(ref _FlightDescription, value); }

        private string _Sound;
        public string Sound { get => _Sound ??= SoundStrategy?.Execute(); private set => SetPropertyValue(ref _Sound, value); }

        public Mallard(IWingedFlightStrategy wingedFlightStrategy, IQuackSoundStrategy quackSoundStrategy)
        {
            Name = "Mallard Duck";
            FlightStrategy = wingedFlightStrategy;
            SoundStrategy = quackSoundStrategy;

        }

        public IFlightStrategy FlightStrategy { get; protected set; }

        public ISoundStrategy SoundStrategy { get; protected set; }
    }
}
