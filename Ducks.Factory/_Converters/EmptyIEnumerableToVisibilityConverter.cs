﻿using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model.Ducks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Ducks.Factory._Converters
{
    public class EmptyIEnumerableToVisibilityConverter : BaseConverter<EmptyIEnumerableToVisibilityConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is IEnumerable enumerable && enumerable.GetEnumerator().MoveNext()) //Can we move to first item? This isdicates there are items
                return Visibility.Visible;

            return Visibility.Hidden; //Hide the element because the collection is empty
            
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
