﻿using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model;
using Ducks.Factory.MVVM.Model.Behaviour;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Ducks.Factory._Converters
{
    public class MakesSoundToVisibilityConverter : BaseConverter<MakesSoundToVisibilityConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is IMakesSound ? Visibility.Visible : Visibility.Collapsed;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
