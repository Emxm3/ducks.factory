﻿using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model.Ducks;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory._Converters
{
    public class IsSameDuckConverter : BaseMultiConverter<IsSameDuckConverter>
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            //Check to see if both ducks are identical
            return values.All(v => v is Duck) && values.All(d => d == values[0]);
                ;

        }

        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
