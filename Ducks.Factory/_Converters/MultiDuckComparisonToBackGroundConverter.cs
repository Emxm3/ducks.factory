﻿using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model.Behaviour;
using Ducks.Factory.MVVM.Model.Ducks;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Ducks.Factory._Converters
{
    public class MultiDuckComparisonToBackGroundConverter : BaseMultiConverter<MultiDuckComparisonToBackGroundConverter>
    {
       
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var red = Color.FromRgb(255, 100, 100);
            var redBrush = new SolidColorBrush(red);
            var green = Color.FromRgb(100, 255, 100);
            var greenBrush = new SolidColorBrush(green);

            //Check to see if both ducks are identicall
            Duck[] ducks = values
                .Where(v => v is Duck) //Make sure we only work on ducks.
                .Select(v => v as Duck) // turn the objects into ducks
                .Take(2) //Get two ducks only
                .ToArray()
                ;

            if (!CompareDuckProperty(ducks, d => d.Name)) return redBrush;          //They must have same name?
            if (!CompareDuckProperty(ducks, d => d.Breed)) return redBrush;         // Are they the same breed?
            if (!CompareDuckProperty(ducks, d => d is ICanFly)) return redBrush;   // Do they have the same flight presence?
            
            if (!CompareDuckProperty(ducks, d => (d as ICanFly)?.FlightStrategy?.GetType())) return redBrush;

            if (!CompareDuckProperty(ducks, d => d is IMakesSound)) return redBrush;
            if (!CompareDuckProperty(ducks, d => (d as IMakesSound)?.SoundStrategy?.GetType())) return redBrush;

            return greenBrush;
        }   
        bool CompareDuckProperty(Duck[] ducks, Func<Duck, object> func)
        {
            var val1 = func(ducks[0]);
            var val2 = func(ducks[1]);

            if (val1 == null && val2 == null) return true;
            
            if (val1 != null && val1.Equals(val2)) return true;


            return val2.Equals(val1);
        }
        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
