﻿using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model.Behaviour;
using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Strategies.Flight;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory._Converters
{
    public class CorrectDuckToBackGroundColorConverter : BaseConverter<CorrectDuckToBackGroundColorConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (!(parameter is Duck target)) return "#0000"; //Needs to be cmpared to another duck

            if (!(value is Duck duck)) return "#f99"; //Has to be a duck

            if (!(duck.GetType() == target.GetType())) return "#f99"; //Has to be the same type
            
            if (!(duck is ICanFly flyable && target is ICanFly)) return "#f99";
            if (!(duck is IMakesSound soundable && target is IMakesSound)) return "#f99";

            if (!(flyable.FlightStrategy == ((ICanFly)target).FlightStrategy)) return "#f99";
            if (!(soundable.SoundStrategy == ((IMakesSound)target).SoundStrategy)) return "#f99";
            
            return "#9f9";

        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
