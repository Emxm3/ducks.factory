﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace Ducks.Factory._wpf
{
    public abstract class BaseConverter<T> : MarkupExtension, IValueConverter where T : new()
    {
        T value;
        public abstract object Convert(object value, Type targetType, object parameter, CultureInfo culture);

        public abstract object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture);

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return value ??= new T();
        }
    }
    public abstract class BaseMultiConverter<T> : MarkupExtension, IMultiValueConverter where T : new()
    {
        T value;

        public abstract object Convert(object[] values, Type targetType, object parameter, CultureInfo culture);

        public abstract object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture);

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return value ??= new T();
        }
    }
}
