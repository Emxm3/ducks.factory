﻿using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Ponds;
using System;
using System.Windows.Input;

namespace Ducks.Factory._Mock
{
    public class MockFarm : IFarm
    {
        public string Title => "Mock Farm";

        public ICommand TransmitDuck => throw new NotImplementedException();

        public event EventHandler<Duck> OnTransmit;
    }
}