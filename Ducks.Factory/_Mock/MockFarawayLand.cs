﻿using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Ponds;
using System;
using System.Windows.Input;

namespace Ducks.Factory._Mock
{
    internal class MockFarawayLand : IMagicalFarAwayLand
    {
        public string Land => "Mock Land";

        public ICommand MagicallyAppear => throw new NotImplementedException();

        public event EventHandler<Duck> OnCreation;

        public void ManifestDuckFromRainbows()
        {
        }
    }
}