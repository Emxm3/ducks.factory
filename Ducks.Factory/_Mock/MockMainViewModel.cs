﻿using Ducks.Factory._wpf;
using Ducks.Factory.MVVM.Model;
using Ducks.Factory.MVVM.Model.Behaviour;
using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.Model.Ponds;
using Ducks.Factory.MVVM.Model.Strategies.Flight;
using Ducks.Factory.MVVM.Model.Strategies.Sound;
using Ducks.Factory.MVVM.ViewModel.Catalogue;
using Ducks.Factory.MVVM.ViewModel.MainWindow;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ducks.Factory._Mock
{
    public class MockMainWindowViewModel : ObservableClass, IMainWindowViewModel
    {
        private ObservableCollection<Pond> _ListOfPonds;
        private Duck _CurrentDuck;

        public Duck CurrentDuck { get => _CurrentDuck; set => SetPropertyValue(ref _CurrentDuck, value); }



        public ICommand Initialize => throw new NotImplementedException();

        public bool Loaded => false;

        public ObservableCollection<Pond> ListOfPonds { get => _ListOfPonds ??= new(); set => SetPropertyValue(ref _ListOfPonds, value); }
        public ObservableCollection<ICatalogueDuck> DuckCatalouge { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IFarm Farm => new MockFarm();

        public ObservableCollection<Duck> ListOfDucks { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IMainWindowViewModel.Loaded { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IMagicalFarAwayLand MagicalFarAwayLand => new MockFarawayLand();

        public Duck TargetDuck { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public MockMainWindowViewModel()
        {
            
        }
    }
}
